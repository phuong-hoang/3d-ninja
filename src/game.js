import * as pc from 'playcanvas'
import { Player } from './player/player'
import { Map } from './map/map'
import { Item, ItemEvent } from './item/item';
import { MiniMap } from './screen/miniMap';
import { EnemyManager } from './enemy/enemyManager';
import { AssetManager, AssetManagerEvent } from './assetManager';
import { Screen2D } from './screen/screen2D';
export const GameEvent = Object.freeze({
    AssetProgress: "gameevent:assetprogress",
    AssetLoaded: "gameevent:assetloaded",
});

class Game extends pc.Application {
    static TotalAsset = 7;
    static AssetLoaded = 0;
    constructor(canvas, useKeyboard = true, useMouse = true) {
        const config = {
            keyboard: useKeyboard ? new pc.Keyboard(window) : null,
            mouse: useMouse ? new pc.Mouse(canvas) : null,
            elementInput: new pc.ElementInput(canvas),
        };

        super(canvas, config);
        this.uiLayer = this.scene.layers.getLayerByName("UI");
        this.worldLayer = this.scene.layers.getLayerByName("World");
        this.skyBoxLayer = this.scene.layers.getLayerByName("Skybox");
        this.assetManager = new AssetManager(this);
        this.assetManager.loadAsset();
        this.preload(() => {
            this.start();
            this.handleLoadAsset();
            this._init();
            this.root.enabled = false;
        });

    }
    _init() {
        this.systems.rigidbody.gravity.set(0, -9.81, 0);
        this.setCanvasFillMode(pc.FILLMODE_FILL_WINDOW);
        this.setCanvasResolution(pc.RESOLUTION_AUTO);
        window.addEventListener("resize", () => this.resizeCanvas());
        // this.scene.exposure =   0.7
        // this.scene.ambientLight = new pc.Color(0.5, 0.5, 0.5);
        this.scene.skyboxIntensity = 0.4
        this.scene.skyboxMip = 2;
        this.scene.setSkybox(this.assets.find("sky").resources);


        this.map = new Map(this)
        this.root.addChild(this.map)
        this.map.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);
        this.map.enabled = false


        this.player = new Player(this)
        this.player.setLocalPosition(0, 0, -4)
        this.root.addChild(this.player)
        this.player.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);
        this.player.enabled = false

        this.item = new Item(this)
        this.root.addChild(this.item)
        this.item.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);
        this.on(ItemEvent.DeleteItem, this.item.deleteItem, this.item)
        this.on(ItemEvent.DisableItem, this.item.disableItem, this.item)
        this.item.enabled = false

        this.listEnemy = new EnemyManager(this, 2)
        this.listEnemy.createEnemy()

        this.miniMap = new MiniMap(this);
        this.miniMap.enabled = false;
        this.root.addChild(this.miniMap)

        this.screen2D = new Screen2D(this);
        this.screen2D.enabled = true;
        this.root.addChild(this.screen2D)

    }
    handleLoadAsset() {
        Game.AssetLoaded += 1;
        // console.log(Game.AssetLoaded)
        let percent = Game.AssetLoaded / Game.TotalAsset;
        this.fire(GameEvent.AssetProgress, percent);
        if (Game.AssetLoaded === Game.TotalAsset) {
            this.fire(GameEvent.AssetLoaded);
        }
    }

}
export { Game }