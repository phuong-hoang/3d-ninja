import * as pc from "playcanvas";
import { EnemyEvent } from "../enemy/enemy";
export class Event {
    constructor(game, player) {
        this.game = game;
        this.player = player;
        this.keyboard = this.game.keyboard;
        this.mouse = this.game.mouse;
        this.mouse = this.game.mouse;
        this.animation = "idle";
        this.onGround = true;
        this.jumping = false;
        this.sound = ""
        this.keyboard.on(pc.EVENT_KEYDOWN, this.keyChange, this);
        this.keyboard.on(pc.EVENT_KEYUP, this.keyChange, this);

        this.mouse.on(pc.EVENT_MOUSEDOWN, this.mouseDown, this);
        this.mouse.on(pc.EVENT_MOUSEUP, this.mouseUp, this);

        this.player.rigidbody.on("collisionstart", (e) => {
            if (e.other.name === "map") {
                this.onGround = true;
                this.jumping = false;
            }
        });

        this.player.rigidbody.on("collisionend", (e) => {
            if (e.name === "map") {
                this.onGround = false;
                this.jumping = true;
            }
        });
    }
    mouseDown(e) {
        let tempAnimation = this.animation;
        if (e.buttons[pc.MOUSEBUTTON_LEFT]) {
            this.animation = "attack";
            if (tempAnimation !== this.animation) {
                // this.player.attack = true;
                this.player.playAnimation(this.animation);
            }
        }
    }
    mouseUp(e) {
        this.animation= "idle"
       
    }
    keyChange() {

        let tempAnimation = this.animation;
        this.checkActions();

        if (tempAnimation !== this.animation) {
            this.player.playAnimation(this.animation);
            if (this.sound != "") {
                if (this.animation == "idle" || this.animation == "jump") {
                    this.sound = "stop"
                }
                this.player.sound(this.sound)
            }
        }
    }
    checkActions() {
        let w = this.keyboard.isPressed(pc.KEY_W);
        let a = this.keyboard.isPressed(pc.KEY_A);
        let s = this.keyboard.isPressed(pc.KEY_S);
        let d = this.keyboard.isPressed(pc.KEY_D);
        let shift = this.keyboard.isPressed(pc.KEY_SHIFT);
        let space = this.keyboard.isPressed(pc.KEY_SPACE);

        if (this.onGround) {
            this.animation = "idle";
            if (w && !s) {
                // this.player.smokeParticle.particlesystem.reset();
                this.player.smokeParticle.particlesystem.play();
                if (shift) {
                    if (a && !d) {
                        this.sound = "walking"
                        this.animation = "left"
                    } else if (d && !a) {
                        this.sound = "walking"
                        this.animation = "right"
                    } else if (space) {
                        this.animation = "jump"
                    } else {
                        this.sound = "running"
                        this.animation = "run";
                    }
                } else {
                    if (a && !d) {
                        this.sound = "walking"
                        this.animation = "left"
                    } else if (d && !a) {
                        this.sound = "walking"
                        this.animation = "right"
                    } else if (space) {
                        this.animation = "jump"
                    } else {
                        this.sound = "walking"
                        this.animation = "walk";
                    }
                }

            } else if (s && !w) {
                // this.player.smokeParticle.particlesystem.reset();
                this.player.smokeParticle.particlesystem.play();
                if (shift) {
                    if (a && !d) {
                        this.sound = "walking"
                        this.animation = "left"
                    } else if (d && !a) {
                        this.sound = "walking"
                        this.animation = "right"
                    } else {
                        this.animation = "runBack";
                        this.sound = "running"
                    }

                } else {
                    if (a && !d) {
                        this.sound = "walking"
                        this.animation = "left"
                    } else if (d && !a) {
                        this.sound = "walking"
                        this.animation = "right"
                    } else {
                        this.sound = "walking"
                        this.animation = "walk";
                    }

                }
            } else if (space) {
                this.animation = "jump"
                this.jumping = true;
            } else if (a && !d) {
                this.sound = "walking"
                this.animation = "left";
            } else if (d && !a) {
                this.sound = "walking"
                this.animation = "right";
            } else {
                // this.player.smokeParticle.particlesystem.reset();
                this.player.smokeParticle.particlesystem.stop();
                this.sound = "stop"
            }
        }

    }

}
