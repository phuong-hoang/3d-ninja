import * as pc from "playcanvas";
import { Control } from "./control";
import { Event } from "./event";
import { ItemEvent } from "../item/item";
import { AssetManager, AssetManagerEvent } from '../assetManager';

export class Player extends pc.Entity {
    constructor(game) {
        super("Player");
        this.game = game;
        this.attack = false
        this.lastFrameCurrentTime = 0;
        this.blendTime = 0.02;
        this.bleed = 0
        this._init();

    }

    _init() {
        this.addComponent("collision", {
            type: "capsule",
            radius: 0.5,
            height: 2,
        });
        this.addComponent("rigidbody", {
            angularDamping: 0,
            angularFactor: pc.Vec3.ZERO,
            friction: 1,
            linearDamping: 0,
            linearFactor: pc.Vec3.ONE,
            mass: 80,
            restitution: 0,
            type: "dynamic",
        });


        this.container = new pc.Entity("container");
        this.addChild(this.container);

        this.entity = new pc.Entity("entity");
        this.game.assets.loadFromUrl(AssetManager.getAssetPath("paladin"), "model", (err, asset) => {
            this.entity.addComponent("model", {
                type: "asset",
                asset: asset,
                castShadows: true,
                layers: [this.game.worldLayer.id]
            });
            this.entity.rotateLocal(0, 180, 0);
            this.entity.setLocalPosition(0, -1, 0);
            this.container.addChild(this.entity);
            this.fire(AssetManagerEvent.AssetLoad);
        })

        this.attackRange = new pc.Entity("attack range")
        this.attackRange.setLocalPosition(0, 0, -2)
        this.attackRange.setLocalScale(0.5, 0.5, 0.5)
        // this.attackRange.addComponent("render",{
        //     type: "box",
        //     castShadows: true
        // })
        this.container.addChild(this.attackRange)

        this.setupcamera()
        this._createHealth()
        new Event(this.game, this)
        this.rigidbody.on("contact", this.onCollision, this)
        this._createParticle()

        this.game.on("update", this.attackEnemy, this)

    }
    attackEnemy() {
        if (this.attack) {
            const result = this.game.systems.rigidbody.raycastFirst(this.container.getPosition(), this.attackRange.getPosition());
            if (result) {
                if (result.entity.name == "enemy") {
                    setTimeout(() => {
                        this.attackParticle.particlesystem.reset();
                        this.attackParticle.particlesystem.play();
                    }, 500);
                    setTimeout(() => {
                        result.entity.bleed("Player")
                    }, 1000);
                }

            }
            this.attack = false
        }
    }
    _createParticle() {
        const scaleCurve = new pc.Curve([0, 0.1]);
        const velocityCurve = new pc.CurveSet([
            [0, 0], // x
            [0, 1], // y
            [0, 0], // z
        ]);
        const velocityCurve2 = new pc.CurveSet([
            [0, 0], // x
            [0, 0.4], // y
            [0, 0], // z
        ]);
        const alphaCurve = new pc.Curve([0, 0.5, 0.5, 1, 1, 0]);

        this.healthParticle = new pc.Entity();
        this.healthParticle.setLocalScale(0.5, 0.5, 0.5)
        this.healthParticle.setLocalPosition(0, this.collision.height / 2, 0);
        this.healthParticle.addComponent("particlesystem", {
            numParticles: 7,
            loop: false,
            lifetime: 0.6,
            rate: 0.2,
            emitterExtents: new pc.Vec3(1.2, 0, 0),
            velocityGraph: velocityCurve,
            // velocityGraph2: velocityCurve2,
            scaleGraph: scaleCurve,
            alphaGraph: alphaCurve,
            colorMapAsset: this.game.assets.find("health"),
        })
        this.container.addChild(this.healthParticle)

        const scaleCurveSmoke =  new pc.Curve([0, 0.1 , 0.5 ,0.2,1,0]);
        const alphaCurve2 = new pc.Curve([0, 1, 0.2, 1, 0.5, 1, 1, 0]);
        this.smokeParticle = new pc.Entity("smoke particle");
        this.smokeParticle.setLocalScale(1.5, 1.5, 1.5)
        this.smokeParticle.setLocalPosition(0, -0.8,0);
        this.smokeParticle.addComponent("particlesystem", {
            numParticles:20,
            lifetime: 1,
            loop: true,
            rate: 0.1,
            colorMapAsset: this.game.assets.find("smoke"),
            initialVelocity: 0.15,
            emitterShape: pc.EMITTERSHAPE_SPHERE,
            emitterRadius: 0.1,
            animLoop: true,
            animTilesX: 2,
            animTilesY: 2,
            animSpeed: 1,
            animIndex: 0,
            animNumFrames: 4,
            autoPlay: true,
            alphaGraph: alphaCurve,
            scaleGraph: scaleCurveSmoke,
        })
        this.container.addChild(this.smokeParticle)

        this.attackParticle = new pc.Entity("attack Particle")
        this.attackParticle.setLocalScale(5, 5, 5)
        this.attackParticle.setLocalPosition(this.attackRange.getLocalPosition().x, this.attackRange.getLocalPosition().y, this.attackRange.getLocalPosition().z + 0.5)
        this.attackParticle.addComponent("particlesystem", {
            numParticles: 1,
            lifetime: 0.5,
            loop: false,
            rate: 0.1,
            colorMapAsset: this.game.assets.find("attackParticle"),
            blendType: pc.BLEND_ADDITIVE,
            animLoop: true,
            animTilesX: 5,
            animTilesY: 5,
            animSpeed: 1,
            animIndex: 0,
            animNumFrames: 3,
            autoPlay: true,
            scaleGraph: scaleCurve,
        })
        this.container.addChild(this.attackParticle)
    }
    _createHealth() {
        this.healthBar = new pc.Entity("healthBar")
        this.healthBar.addComponent("model", {
            type: "box",
            castShadows: false,
            layers: [this.game.worldLayer.id]
        })
        this.healthBar.setLocalPosition(0, 1, 0);
        this.healthBar.setLocalScale(0.8, 0.08, 0.01);
        this.container.addChild(this.healthBar)
        this.health = new pc.Entity("health");
        this.health.addComponent("model", {
            type: "box",
            castShadows: false,
            layers: [this.game.worldLayer.id]
        });
        let redMaterial = new pc.StandardMaterial();
        redMaterial.diffuse = pc.Color.RED;
        redMaterial.update();
        this.health.model.meshInstances[0].material = redMaterial;
        this.health.setLocalPosition(0, -0.015, 1)
        this.health.setLocalScale(0.95, 0.767, 0.01)
        this.healthBar.addChild(this.health)

    }
    bleedPlayer(name){
        const now = Date.now();
        if (this.health.getLocalPosition().x <= -0.45) {
            console.log("die");
        }
        else if (this.lasCollidedEntity !== name || now - this.lastCollidedTime > 500) {
            this.health.setLocalPosition(this.health.getLocalPosition().x - 0.1 / 4, -0.015, 1)
            this.health.setLocalScale(this.health.getLocalScale().x - 0.1/2, 0.767, 0.01)
            this.lasCollidedEntity = name;
            this.lastCollidedTime = now;
        }
    }
    onCollision(e) {
        const now = Date.now();
        if (this.health.getLocalPosition().x <= -0.45) {
            console.log("die");
        }
        else if (e.other.name === "growUp") {
            if (this.lasCollidedEntity !== e.other || now - this.lastCollidedTime > 700) {
                this.bleed += 0.1
                this.healthBar.setLocalPosition(0, 1 + this.bleed, 0);

                this.collision.radius = 0.5 + this.bleed / 2
                this.collision.height = 2 + this.bleed * 2


                this.entity.setLocalScale(1 + this.bleed, 1 + this.bleed, 1 + this.bleed);
                this.entity.setLocalPosition(this.entity.getLocalPosition().x, -1 - this.bleed, this.entity.getLocalPosition().z)

                this.camera.setLocalPosition(0, 3 + this.bleed, 5 + this.bleed)
                this.game.fire(ItemEvent.DeleteItem, "growUp")
                this.lasCollidedEntity = e.other;
                this.lastCollidedTime = now;
            }
        } else if (e.other.name === "heart") {
            if (this.lasCollidedEntity !== e.other || now - this.lastCollidedTime > 700) {
                if (this.health.getLocalPosition().x + 0.1 / 2 <= 0) {
                    this.health.setLocalPosition(this.health.getLocalPosition().x + 0.1 / 2, -0.015, 1)
                    this.health.setLocalScale(this.health.getLocalScale().x + 0.1, 0.767, 0.01)

                    this.healthParticle.particlesystem.reset();
                    this.healthParticle.setLocalPosition(0, this.collision.height / 2, 0);
                    this.healthParticle.particlesystem.play();

                    this.game.fire(ItemEvent.DeleteItem, "health")
                    this.lasCollidedEntity = e.other;
                    this.lastCollidedTime = now;
                } else {
                    this.game.fire(ItemEvent.DisableItem, "health")

                }
            }
        }

    }
    onControl() {
        new Control(this.game, this)
    }
    setupcamera() {
        this.camera = new pc.Entity("cam1");
        this.camera.addComponent("camera", {
            clearColor: new pc.Color(0.2, 0.2, 0.2),
            fov: 60,
            farClip: 100000,
            layers: [this.game.uiLayer.id, this.game.worldLayer.id, this.game.skyBoxLayer.id]
        });

        // this.camera.translate(0, 0.5, 4);
        this.camera.setLocalPosition(0, 3, 5);
        // this.camera.setLocalPosition(0,2, -1);
        this.camera.setRotation(-0.2, 0, 0, 1)
        this.container.addChild(this.camera)
        this.camera.enabled = true

        this.camera2 = new pc.Entity("cam2");
        this.camera2.addComponent("camera", {
            fov: 60,
            farClip: 100000,
            clearColor: new pc.Color(0.2, 0.2, 0.2),
            layers: [this.game.uiLayer.id, this.game.worldLayer.id, this.game.skyBoxLayer.id]
        });
        this.container.addChild(this.camera2)
        this.camera2.enabled = false

    }
    playAnimation(animation) {
        if (!this.entity.animation) {
            this.entity.addComponent("animation", {
                loop: true,
            });
        }
        if (!this.entity.animation.getAnimation(animation)) {
            let tmp = this.entity.animation.assets;
            tmp.push(this.game.assets.find(animation));
            this.entity.animation.assets = tmp;
        }
        if (animation == "attack") {
            if (this.entity.animation.currentTime == this.entity.animation.getAnimation(animation).duration || this.lastFrameCurrentTime > this.entity.animation.currentTime) {
                this.attack = true
                this.entity.animation.loop = false;
                this.entity.animation.speed = 1;
                this.entity.animation.play(animation, 0.001);
            }
            this.lastFrameCurrentTime =  this.entity.animation.currentTime; 
        }
        else if (animation == "jump") {
            this.entity.animation.loop = false;
            this.entity.animation.speed = 1;
            this.entity.animation.play(animation, 0.001);
        }
        else if (animation == "runBack") {
            this.entity.animation.loop = true;
            this.entity.animation.speed = -1;
            this.entity.animation.play(animation, this.blendTime);
        } else {
            this.entity.animation.loop = true;
            this.entity.animation.speed = 1;
            this.entity.animation.play(animation, this.blendTime);
        }

    }
    sound(sound) {
        if (!this.entity.sound) {
            this.entity.addComponent("sound");
        }
        if (sound == "stop") {
            this.entity.sound.enabled = false
        } else {
            if (this.entity.sound.slot(this.slotOld)) {
                this.entity.sound.removeSlot(this.slotOld)
            }
            let pitch = 0.6
            if (sound == "walking") {
                pitch = 0.6
            } else {
                pitch = 0.9
            }
            this.entity.sound.enabled = true
            if (!this.entity.sound.slot(sound)) {
                this.entity.sound.addSlot(sound, {
                    loop: true,
                    asset: this.game.assets.find(sound),
                    pitch: pitch,
                });
                this.slotOld = sound;
            }
        }
        this.entity.sound.play(sound);
    }


}
