import * as pc from 'playcanvas'
export class Control {
    constructor(game, player, entity) {
        this.game = game
        this.player = player
        this.entity = entity
        this.old = this.player.camera.getLocalPosition();

        this.container = player.findByName("container")
        this.power = 5
        this.lookSpeed = 0.1;

        this.force = new pc.Vec3();
        this.eulers = new pc.Vec3();
        this.onGround = true;
        this.jumping = false;
        this.down = 0
        this.firstCamera = false
        this.canvas = this.game.graphicsDevice.canvas;

        game.mouse.on("mousemove", this._onMouseMove, this);

        game.mouse.on(
            "mousedown",
            function () {
                game.mouse.enablePointerLock();
            },
            this
        );

        game.on("update", this.control, this)


        this.player.rigidbody.on("collisionstart", (e) => {
            if (e.other.name === "map") {
                this.onGround = true;
                this.jumping = false;
            }
        });

        this.player.rigidbody.on("collisionend", (e) => {
            if (e.name === "map") {
                this.onGround = false;
                this.jumping = true;
            }
        });

    }
    control() {
        var forward = this.container.forward;
        var right = this.container.right;
        this.x = 0, this.y = 0;
        this.z = 0;

        if (this.game.keyboard.isPressed(pc.KEY_SHIFT)) {
            this.power = 10;
        }
        else {
            this.power = 5;
        }
        // Use W-A-S-D keys to move player
        // Check for key presses
        if (this.game.keyboard.isPressed(pc.KEY_A)) {
            // if (this.firstCamera) {
            this.x -= right.x;
            this.z -= right.z;
            // }else{
            //     this.goLeft= true;
            // }
            // this.x -= 0.1; 
        }
        if (this.game.keyboard.isPressed(pc.KEY_D)) {
            // if (this.firstCamera) {
            this.x += right.x;
            this.z += right.z;
            // }else{
            //     this.goRight = true
            // }
            // this.x += 0.1;

        }

        if (this.game.keyboard.isPressed(pc.KEY_W)) {
            this.x += forward.x;
            this.z += forward.z;
            // this.z -=0.1

        }

        if (this.game.keyboard.isPressed(pc.KEY_S)) {
            this.x -= forward.x;
            this.z -= forward.z;
            // this.z += 0.1;

        }
        // if(this.goRight){
        //     this.eulers.x -= 2
        //     this.goRight= false
        // }
        // if (this.goLeft) {
        //     this.eulers.x += 2
        //     this.goLeft=false
        // }

        // if (this.eulers.x>= 360) {
        //     this.eulers.x -= 360
        // }else if(this.eulers.x<= -360){
        //     this.eulers.x += 360
        // }

        if (this.game.mouse.isPressed(pc.MOUSEBUTTON_RIGHT)) {
            this.firstCamera = true
        }

        // console.log(this.cam1)
        if (this.firstCamera) {
            this.player.camera.enabled = false
            this.player.camera2.enabled = true
            this.player.camera2.setLocalPosition(0, this.player.camera.getLocalPosition().y / 6, -1);
            this.container.setEulerAngles(0, this.eulers.x, 0);
            if (this.eulers.y < -90) {
                this.eulers.y = -90
            } else if (this.eulers.y > 90) {
                this.eulers.y = 90
            }
            this.player.camera2.setEulerAngles(this.eulers.y, this.eulers.x, 0);
        } else {
            if (this.eulers.y < -90) {
                this.eulers.y = -90
            } else if (this.eulers.y > 90) {
                this.eulers.y = 90
            }
            this.container.setEulerAngles(0, this.eulers.x, 0);
            this.player.camera.setEulerAngles(this.eulers.y, this.eulers.x, 0);
        }
        if (this.game.mouse.isPressed(pc.MOUSEBUTTON_MIDDLE)) {
            this.player.camera.enabled = true
            this.player.camera2.enabled = false
            this.firstCamera = false
        }
        // this.container.setEulerAngles(0, this.eulers.x, 0);
        // this.player.setPosition(this.x,0,this.z)

        // this.player.camera.setEulerAngles(this.eulers.y, this.eulers.x, 0);
        // this.player.camera.setLocalPosition(this.old.x,this.old.y,this.old.z)
        if (this.x !== 0 && this.z !== 0) {
            this.force.set(this.x, 0, this.z).normalize().scale(this.power);
            // console.log(this.force.set(this.x, 0, this.z).normalize().scale(this.power))
            // this.player.setPosition(this.x,0,this.z)
            if (this.onGround && !this.jumping) {
                this.player.rigidbody.linearVelocity = this.force;
            } else {
                this.player.rigidbody.applyForce(this.force.scale(20));
            }
        } else {
            if (!this.jumping) {
                this.force.set(0, 0, 0)
                this.player.rigidbody.linearVelocity = this.force
            }

        }

        if (
            this.game.keyboard.isPressed(pc.KEY_SPACE) &&
            this.onGround &&
            !this.jumping
        ) {
            this.down = 10
            this.force.set(0, this.down, 0).normalize().scale(4);
            this.player.rigidbody.linearVelocity = this.force;
            this.jumping = true;
        }

    }
    _onMouseMove(e) {
        if (pc.Mouse.isPointerLocked() || e.buttons[0]) {
            this.eulers.x -= this.lookSpeed * e.dx;
            this.eulers.y -= this.lookSpeed * e.dy;
        }
    }
}