import * as pc from "playcanvas";

export class Screen2D extends pc.Entity {
    constructor(game) {
        super("screen2D");
        this.game = game;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
        });
        this._init();
    }

    _init() {
        this.camera = new pc.Entity();
        this.camera.addComponent("camera", {
            clearColor: new pc.Color(0, 0, 0),
            layers: [this.game.uiLayer.id]
        });

        this.addChild(this.camera);
        this.group = new pc.Entity("group");
        this.group.addComponent("element", {
            type: pc.ELEMENTTYPE_GROUP,
            anchor: new pc.Vec4(0.5, 0.5, 0.5, 0.5),
            pivot: new pc.Vec2(0.5, 0.5),
            // layers: [this.game.uiLayer.id]
        })
        this.addChild(this.group);

        this.background = new pc.Entity("background");
        this.background.addComponent("element", {
            type: pc.ELEMENTTYPE_IMAGE,
            width: 800,
            height: 600,
            anchor: new pc.Vec4(0.5, 0.5, 0.5, 0.5),
            pivot: new pc.Vec2(0.5, 0.5),
            opacity: 1,
            // layers: [this.game.uiLayer.id],
            textureAsset: this.game.assets.find("background")
        })
        this.group.addChild(this.background)

        this.button = new pc.Entity("button");
        this.button.addComponent("button", {
            imageEntity: this.button
        })
        this.button.addComponent("element", {
            type: pc.ELEMENTTYPE_IMAGE,
            width: 250,
            height: 60,
            anchor: new pc.Vec4(0.5, 0.5, 0.5, 0.5),
            pivot: new pc.Vec2(0.5, 0.5),
            opacity: 1,
            // layers: [this.game.uiLayer.id],
            useInput: true,
            textureAsset: this.game.assets.find("button")
        })
        this.button.setLocalPosition(0, -220, 0)

        this.button.button.on("click", () => {
            this.game.map.enabled = true;
            this.game.player.enabled = true
            this.game.player.onControl()
            this.game.listEnemy.enabledEnemy()
            this.game.item.enabled = true
            this.game.item.playAnimation()
            this.game.miniMap.enabled = true;
            this.enabled = false
        })
        this.group.addChild(this.button)

    }


}
