import * as pc from "playcanvas";

export class MiniMap extends pc.Entity {
    constructor(game) {
        super("MiniMap");
        this.game = game;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
        });
        this.player = this.game.root.findByName("Player")
        this._init();
    }

    _init() {
        this.textureMap = new pc.Texture(this.game.graphicsDevice, {
            width: 412,
            height: 256,
            format: pc.PIXELFORMAT_R8_G8_B8,
            mipmaps: true,
            minFilter: pc.FILTER_LINEAR,
            magFilter: pc.FILTER_LINEAR,
            addressU: pc.ADDRESS_CLAMP_TO_EDGE,
            addressV: pc.ADDRESS_CLAMP_TO_EDGE,
        })
        this.renderTarget = new pc.RenderTarget({
            colorBuffer: this.textureMap,
            depth: true,
            flipY: true,
        });
        this.cameraMiniMap = new pc.Entity("camera minimap");
        this.cameraMiniMap.addComponent("camera", {
            layers: [this.game.uiLayer.id, this.game.worldLayer.id, this.game.skyBoxLayer.id],
            priority: -1,
            renderTarget: this.renderTarget,
            clearColor: new pc.Color(0.2, 0.2, 0.2),
        });

        this.viewMap = new pc.Entity("viewMap");
        this.viewMap.addComponent("element", {
            type: pc.ELEMENTTYPE_IMAGE,
            width: 412,
            height: 256,
            anchor: pc.Vec4.ONE,
            pivot: pc.Vec2.ONE,
            opacity: 1,
            texture: this.textureMap
        })
        this.addChild(this.viewMap)
        this.game.on("update", () => {
            const euler = this.player.container.getLocalEulerAngles();
            const pos = this.player.getLocalPosition()
            this.cameraMiniMap.setRotation(-0.5, 0, 0, 1)
            this.cameraMiniMap.setPosition(pos.x, pos.y + 2, pos.z);
            this.cameraMiniMap.setEulerAngles(euler.x, euler.y, euler.z)
        })
        this.game.root.addChild(this.cameraMiniMap)

    }


}
