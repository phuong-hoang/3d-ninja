import { Game, GameEvent } from './game'
import { loadAmmo, loadDraco } from './loadModule';
const container = document.querySelector(".container");
const progress = document.getElementById("progress-value");
const canvas = document.getElementById("canvas");
window.onload = () => {
    loadAmmo().then(() => {
        run();
    });
};

function run() {
    const app = new Game(canvas);
    app.on(GameEvent.AssetProgress, setProgress);
    app.on(GameEvent.AssetLoaded, function () {
        container.style.display = "none";
        canvas.style.display = "inline";
        app.root.enabled = true;
    });
}

function setProgress(value) {
    progress.style.width = value * 100 + "%";
}