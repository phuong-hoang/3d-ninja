import * as pc from "playcanvas";
import { AssetManager , AssetManagerEvent} from '../assetManager';

export class Arrow extends pc.Entity {
    constructor(game) {
        super("arrow");
        this.game = game;
        this.blendTime = 0;
        this._init();
        
    }

    _init() {
        this.game.assets.loadFromUrl(AssetManager.getAssetPath("arrow") , "model", (err, asset)=>{
            this.addComponent("model", {
                type: "asset",
                asset: asset,
                castShadows: true,
                layers:[this.game.worldLayer.id]
            });
        })
        
    }
    deleteItem(item){
        
    }
    disableItem(item){
    }

}
