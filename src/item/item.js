import * as pc from "playcanvas";
import { AssetManager, AssetManagerEvent } from '../assetManager';
export const ItemEvent = Object.freeze({
    DeleteItem: "itemevent:deleteitem",
    DisableItem: "itemevent:disableItem"
})
export class Item extends pc.Entity {
    constructor(game) {
        super("item");
        this.game = game;
        this.blendTime = 0;
        this._init();

    }

    _init() {

        // const url = "assets/model/Map/Map.glb";
        this.setLocalPosition(0, -0.5, 0)

        this.heart = new pc.Entity("heart");
        this.addChild(this.heart)

        this.heart.setLocalPosition(5, 0, 2)
        this.heart.setLocalScale(6, 6, 6)
        this.heart.addComponent("animation", {
            loop: true,
        })
        // this.lightHeart = new pc.Entity("light heart")
        // this.lightHeart.setLocalPosition(0,)
        // this.lightHeart.addComponent("light",{
        //     type: "point",
        //     intensity: 4,
        //     range:0.7,
        //     color: new pc.Color(255,0,0)
        // })

        let tmp = this.heart.animation.assets;
        tmp.push(this.game.assets.find("rotation"));
        this.heart.animation.assets = tmp

        this.game.assets.loadFromUrl(AssetManager.getAssetPath("heart"), "model", (err, asset) => {
            this.heart.addComponent("model", {
                type: "asset",
                asset: asset,
                layers: [this.game.worldLayer.id]
            });

            this.heart.addComponent("collision", {
                type: "mesh",
                asset: asset,
            });
            this.heart.addComponent("rigidbody", {
                type: "static",
                restitution: 0.5,
                friction: 0.9,
            });
            this.fire(AssetManagerEvent.AssetLoad)
        })

        this.growUp = new pc.Entity("growUp");
        this.addChild(this.growUp)

        this.growUp.setLocalPosition(7, 0.2, 5)

        this.game.assets.loadFromUrl(AssetManager.getAssetPath("growUp"), "model", (err, asset) => {
            this.growUp.addComponent("model", {
                type: "asset",
                asset: asset,
                layers: [this.game.worldLayer.id]
            });

            this.growUp.addComponent("collision", {
                type: "mesh",
                asset: asset,
            });
            this.growUp.addComponent("rigidbody", {
                type: "static",
                restitution: 0.5,
                friction: 0.9,
            });
            this.fire(AssetManagerEvent.AssetLoad)
        })
        this.game.on("update", (dt) => {
            this.growUp.rotate(0, -30 * dt, 0)
        })

    }
    playAnimation() {
        this.heart.animation.play("rotation");
    }
    deleteItem(item) {
        if (item == "health") {
            this.heart.rigidbody.teleport(Math.floor(Math.random() * 10), 0, Math.floor(Math.random() * 10));
            this.heart.linearVelocity = pc.Vec3.ZERO;
            this.heart.rigidbody.angularVelocity = pc.Vec3.ZERO;
        } else if (item == "growUp") {
            this.growUp.rigidbody.teleport(Math.floor(Math.random() * 10), 0, Math.floor(Math.random() * 10));
            this.growUp.linearVelocity = pc.Vec3.ZERO;
            this.growUp.rigidbody.angularVelocity = pc.Vec3.ZERO;
        }

    }
    disableItem(item) {
        if (item == "health") {
            this.heart.collision.enabled = false
            this.heart.rigidbody.enabled = false
            setTimeout(() => {
                this.heart.collision.enabled = true
                this.heart.rigidbody.enabled = true
            }, 1000);
        }
    }

}
