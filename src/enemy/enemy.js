import * as pc from "playcanvas";
import { AssetManager, AssetManagerEvent } from '../assetManager';
export const EnemyEvent = Object.freeze({
    Bleed: "enemyevent:bleed"
})
export class Enemy extends pc.Entity {
    constructor(game, pos) {
        super("enemy");
        this.pos = pos
        this.game = game;
        this.player = this.game.player
        this.force = new pc.Vec3();
        this.attackPlayer= false
        this.collisionPlayer = false
        this.oldAnimation = ""
        this._init();

    }

    _init() {
        this.setLocalPosition(this.pos)
        this.addComponent("collision", {
            type: "capsule",
            radius: 0.5,
            height: 2,
        });
        this.addComponent("rigidbody", {
            angularDamping: 0,
            angularFactor: pc.Vec3.ZERO,
            friction: 1,
            linearDamping: 0,
            linearFactor: pc.Vec3.ONE,
            mass: 80,
            restitution: 0,
            type: "dynamic",
        });


        this.containerEnemy = new pc.Entity("container");
        this.addChild(this.containerEnemy);

        this.entityEnemy = new pc.Entity("entity");
        this.game.assets.loadFromUrl(AssetManager.getAssetPath("enemy"), "model", (err, asset) => {
            this.entityEnemy.addComponent("model", {
                type: "asset",
                asset: asset,
                castShadows: true,
                layers: [this.game.worldLayer.id]
            });
            this.entityEnemy.rotateLocal(0, 180, 0);
            this.entityEnemy.setLocalPosition(0, -1, 0);
            this.containerEnemy.addChild(this.entityEnemy);
            this.fire(AssetManagerEvent.AssetLoad);
        })

        this.attackRangeEnemy = new pc.Entity("attack range")
        this.attackRangeEnemy.setLocalPosition(0, 0, -2)
        this.attackRangeEnemy.setLocalScale(0.5, 0.5, 0.5)
        // this.attackRangeEnemy.addComponent("render",{
        //     type: "box",
        //     castShadows: true
        // })
        this.entityEnemy.addComponent("animation", {
            loop: true,
        })
        this.listAnimation = this.entityEnemy.animation.assets;
        this.listAnimation.push(this.game.assets.find("idle_enemy"));
        this.listAnimation.push(this.game.assets.find("walk_enemy"));
        this.listAnimation.push(this.game.assets.find("attack_enemy"));
        this.entityEnemy.animation.assets = this.listAnimation

        this.containerEnemy.addChild(this.attackRangeEnemy)
        this.createHeart()
        this._createParticle()
        this.game.on("update", () => {
            this.move()
        })
        this.game.on("update", this.onAttackPlayer, this)
        this.rigidbody.on("contact", this.onCollisionPlayer, this)

    }
    _createParticle() {
        const scaleCurve = new pc.Curve([0, 0.1]);
        this.attackPlayerParticle = new pc.Entity("attack")
        this.attackPlayerParticle.setLocalScale(5, 5, 5)
        this.attackPlayerParticle.setLocalPosition(this.attackRangeEnemy.getLocalPosition().x, this.attackRangeEnemy.getLocalPosition().y, this.attackRangeEnemy.getLocalPosition().z )
        this.attackPlayerParticle.addComponent("particlesystem", {
            numParticles: 1,
            lifetime: 0.5,
            loop: false,
            rate: 0.1,
            colorMapAsset: this.game.assets.find("attackParticle"),
            blendType: pc.BLEND_ADDITIVE,
            animLoop: true,
            animTilesX: 5,
            animTilesY: 5,
            animSpeed: 1,
            animIndex: 0,
            animNumFrames: 3,
            autoPlay: true,
            scaleGraph: scaleCurve,
        })
        this.containerEnemy.addChild(this.attackPlayerParticle)
    }
    onAttackPlayer(){
        if (this.attackPlayer) {
            const result = this.game.systems.rigidbody.raycastFirst(this.containerEnemy.getPosition(), this.attackRangeEnemy.getPosition());
            if (result) {
                // console.log(result.entity.name)
                if (result.entity.name == "Player") {
                    // setTimeout(() => {
                    //     this.attackPlayerParticle.particlesystem.reset();
                    //     this.attackPlayerParticle.particlesystem.play();
                    // }, 1000);
                    setTimeout(() => {
                        result.entity.bleedPlayer("enemy")
                    }, 700);
                }

            }
            
        }
    }
    onCollisionPlayer(e) {
        if (e.other.name === "Player") {
            this.collisionPlayer = true
            this.force.set(0, 0, 0)
            this.rigidbody.linearVelocity = this.force
            if (this.oldAnimation === "walk_enemy") {
                this.entityEnemy.animation.play("attack_enemy")
                this.oldAnimation = "attack_enemy"
                this.attackPlayer= true
            }
            setTimeout(() => {
                if (this.oldAnimation !== "walk_enemy") {
                    this.entityEnemy.animation.play("walk_enemy")
                    this.oldAnimation = "walk_enemy"
                    this.attackPlayer = false
                }
                this.collisionPlayer = false  
            }, 10000);
        }
    }
    firstAnimation() {
        this.entityEnemy.animation.play("idle_enemy");
        setTimeout(() => {
            this.entityEnemy.animation.play("walk_enemy");
            this.oldAnimation = "walk_enemy"
        }, 2000);
    }
    move() {
        if (!this.enemyDead) {
            if (!this.collisionPlayer) {
                var position = this.player.getPosition();
                position.y = 0.5
                this.containerEnemy.lookAt(position);
                this.force.copy(this.containerEnemy.forward);
                this.force.y = 0;
                this.force.normalize();
                this.rigidbody.linearVelocity = this.force;
                this.healthBar.setEulerAngles(0, this.containerEnemy.getEulerAngles().y, 0)
            } 
        }

    }
    createHeart() {
        this.healthBar = new pc.Entity("healthBar")
        this.healthBar.addComponent("model", {
            type: "box",
            castShadows: false,
            layers: [this.game.worldLayer.id]
        })
        this.healthBar.setLocalPosition(0, 1, 0);
        this.healthBar.setLocalScale(0.8, 0.08, 0.01);
        this.addChild(this.healthBar)
        this.health = new pc.Entity("health");
        this.health.addComponent("model", {
            type: "box",
            castShadows: false,
            layers: [this.game.worldLayer.id]
        });
        let redMaterial = new pc.StandardMaterial();
        redMaterial.diffuse = pc.Color.RED;
        redMaterial.update();
        this.health.model.meshInstances[0].material = redMaterial;
        this.health.setLocalPosition(0, -0.015, 0.5)
        this.health.setLocalScale(0.95, 0.767, 4)
        this.healthBar.addChild(this.health)
    }
    dead() {
        this.enemyDead = true;
        this.listAnimation.push(this.game.assets.find("dead_enemy"))
        this.entityEnemy.animation.assets = this.listAnimation
        this.entityEnemy.animation.loop = false
        this.entityEnemy.animation.play("dead_enemy");
        setTimeout(() => {
            this.removeChild(this.healthBar)
            this.enabled = false

        }, 3000);
    }
    bleed(name) {
        const now = Date.now();
        if (this.health.getLocalPosition().x <= -0.44) {
            this.dead()
        } else if (this.lasCollidedEntity !== name || now - this.lastCollidedTime > 500) {
            this.health.setLocalPosition(this.health.getLocalPosition().x - 0.1, -0.015, 0.5)
            this.health.setLocalScale(this.health.getLocalScale().x - 0.1 * 2, 0.767, 4)
            this.lasCollidedEntity = name;
            this.lastCollidedTime = now;
        }
    }

}
