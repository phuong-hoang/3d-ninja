import * as pc from "playcanvas";
import { Enemy } from "./enemy";
import { EnemyEvent } from "./enemy";
import { AssetManager, AssetManagerEvent } from '../assetManager';

export class EnemyManager {
    constructor(game, numEnemy) {
        this.numEnemy = numEnemy
        this.game = game;
        this.listEnemy = []
    }
    createEnemy() {

        let pos = new pc.Vec3();
        while (this.numEnemy > 0) {
            pos.set(
                Math.floor(Math.random() * 10),
                0,
                Math.floor(Math.random() * 10)
            );
            let enemy = new Enemy(this.game, pos)
            enemy.enabled = false
            this.listEnemy.push(enemy)
            enemy.on(AssetManagerEvent.AssetLoad, this.game.handleLoadAsset, this.game);
            // this.game.on(EnemyEvent.Bleed, enemy.bleed , enemy)
            this.game.root.addChild(enemy)
            this.numEnemy--
        }

    }
    enabledEnemy() {
        this.listEnemy.forEach((enemy) => {
            enemy.enabled = true
            enemy.firstAnimation()
        })
    }

}
