import * as pc from 'playcanvas'
import listAssets from "./assets.json";

export const AssetManagerEvent = Object.freeze({
    AssetLoad: "assetmanagerevent:assetload"
})

export class AssetManager {
    constructor(app) {
        this.app = app;
    }

    loadAsset() {
        listAssets.forEach((raw) => {
            if (raw.type === "model") return;
            let path = `assets/${raw.path}`;
            let asset = new pc.Asset(raw.name, raw.type, {
                url: path,
            });
            asset.preload = true;
            this.app.assets.add(asset);
            this.app.assets.load(asset);
        });
    }

    static getAssetPath(name) {
        for (let raw of listAssets) {
            if (raw.name === name) {
                let path = `assets/${raw.path}`;
                return path;
            }
        }
    }
}