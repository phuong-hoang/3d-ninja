import * as pc from "playcanvas";
import { Light } from "./light";
import { AssetManager, AssetManagerEvent } from '../assetManager';

export class Map extends pc.Entity {
    constructor(game) {
        super("map");
        this.game = game;
        this._init();

    }

    _init() {
        this.light = new Light(this.game)
        this.game.root.addChild(this.light);

        this.setLocalPosition(0, -0.5, 0)
        this.setLocalScale(0.4, 0.4, 0.4);
        this.world = new pc.Entity("world")
        this.addChild(this.world)
        this.game.assets.loadFromUrl(AssetManager.getAssetPath("map"), "model", (err, asset) => {
            this.world.addComponent("model", {
                type: "asset",
                asset: asset,
                layers: [this.game.worldLayer.id]
            });

            this.addComponent("collision", {
                type: "mesh",
                asset: asset,
            });
            this.addComponent("rigidbody", {
                type: "static",
                restitution: 0,
                friction: 1,
            });
            this.fire(AssetManagerEvent.AssetLoad);
        })



    }

}
