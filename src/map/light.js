import * as pc from "playcanvas";

export class Light extends pc.Entity {
    constructor(game) {
        super("light parent");
        this.game = game;
        this.far = 500
        this.speed = 1
        this.time = 0
        this._init();

    }

    _init() {
        this.translate(0, 0, 0);
        this.light = new pc.Entity();
        this.light.addComponent("light", {
            type: "directional",
            intensity: 1,
            castShadows: true,
            innerConeAngle: 80,
            outerConeAngle: 85,
            shadowBias: 0.2,

            normalOffsetBias: 0.1,
            shadowResolution: 2048,
            layers: [this.game.uiLayer.id, this.game.worldLayer.id]
        });

        this.light.setLocalScale(0.2, 0.2, 0.2);
        this.addChild(this.light);

        const brightMaterial = new pc.StandardMaterial();
        brightMaterial.emissive = new pc.Color(0.7, 0.7, 1);
        brightMaterial.useLighting = false;
        brightMaterial.cull = pc.CULLFACE_BACK;
        brightMaterial.update();

        const brightShape = new pc.Entity();
        brightShape.addComponent("render", {
            type: "sphere",
            material: brightMaterial,
            castShadows: false,
        });

        brightShape.setLocalScale(50, 50, 50);
        this.addChild(brightShape);

        this.game.on("update", this.update, this)
    }
    update(dt) {
        if (this.game.keyboard.isPressed(pc.KEY_T)) {
            if (this.speed < 7) {
                this.speed += 0.1;
            } else {
                this.speed = 7
            }

        }
        else {
                this.speed = 0.5
        }
        this.time += dt * this.speed;
        const factor = (Math.sin(this.time * 0.06) + 1) * 0.5;
        this.setLocalEulerAngles(
            pc.math.lerp(360, 0, factor),
            0,
            180
        );
        const dir = this.getWorldTransform().getY();
        // console.log(this.getLocalEulerAngles().x )   
        this.setPosition(
            dir.x * this.far,
            dir.y * this.far,
            dir.z * this.far
        );
        if (this.getLocalEulerAngles().x <= 95 && this.getLocalEulerAngles().x >= 0) {
            this.light.enabled = false
            if (this.game.scene.skyboxIntensity <= 0.4 && this.game.scene.skyboxIntensity > 0.1) {
                this.game.scene.skyboxIntensity -= 0.01
            }

        }
        else if (this.getLocalEulerAngles().x >= -85 && this.getLocalEulerAngles().x <= -0) {
            this.light.enabled = false
            if (this.game.scene.skyboxIntensity <= 0.4 && this.game.scene.skyboxIntensity > 0.1) {
                this.game.scene.skyboxIntensity -= 0.01
            }
        } else {
            this.light.enabled = true
            if (this.game.scene.skyboxIntensity < 0.4) {
                this.game.scene.skyboxIntensity += 0.01
            }
        }
    }

}
